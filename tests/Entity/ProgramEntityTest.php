<?php

namespace App\Tests\Entity;

use App\Entity\Program;
use DateTime;
use PHPUnit\Framework\TestCase;

class ProgramEntityTest extends TestCase
{
    public function testSetLabel() :void
    {
        $program = new Program();
        $newLabel = 'Program';
        $program->setLabel($newLabel);
        self::assertSame($newLabel, $program->getLabel());
    }

    public function testSetBudget() :void
    {
        $program = new Program();
        $newBudget = 1000;
        $program->setBudget($newBudget);
        self::assertSame($newBudget, $program->getBudget());
    }

    public function testSetDateStart() :void
    {
        $program = new Program();
        $newDateStart = new DateTime();
        $program->setDateStart($newDateStart);
        self::assertSame($newDateStart, $program->getDateStart());
    }

    public function testSetDateEnd() :void
    {
        $program = new Program();
        $newDateEnd = new DateTime();
        $program->setDateStart($newDateEnd);
        self::assertSame($newDateEnd, $program->getDateStart());
    }
}