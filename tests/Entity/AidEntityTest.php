<?php

namespace App\Tests\Entity;

use App\Entity\Aid;
use App\Entity\Program;
use PHPUnit\Framework\TestCase;

class AidEntityTest extends TestCase
{
    public function testSetLabel() :void
    {
        $aid = new Aid();
        $newLabel = 'MaPrimeRenov';
        $aid->setLabel($newLabel);
        self::assertSame($newLabel, $aid->getLabel());
    }

    public function testSetAmount() :void
    {
        $aid = new Aid();
        $newAmount = 1000;
        $aid->setAmount($newAmount);
        self::assertSame($newAmount, $aid->getAmount());
    }

    public function testSetProgram() :void
    {
        $aid = new Aid();
        $newProgram = new Program();
        $aid->setProgram($newProgram);
        self::assertSame($newProgram, $aid->getProgram());
    }
}