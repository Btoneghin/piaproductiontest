<?php
namespace App\Tests\Api;

use ApiPlatform\Symfony\Bundle\Test\ApiTestCase;
use App\Entity\Program;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class AidApiTest extends ApiTestCase
{
    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     */
    public function testPostItem(): void
    {
        $program = $this->getProgram();

        $response = static::createClient()->request(
            'POST',
            '/api/aids',
            [
                'headers' => [
                        'Content-Type' => 'application/json'
                ],
                'body' => json_encode(
                    [
                        'label' => 'Create new aid',
                        'amount' => 1000,
                        'program' => '/api/programs/' . $program->getId(),
                    ]
            )]
        );

        $this->assertResponseIsSuccessful();
        $this->assertJsonContains([
            "@context" => "/api/contexts/Aid",
            "@type" => "Aid",
            "label" => "Create new aid",
            "amount" => 1000,
            "program" => '/api/programs/' . $program->getId()
        ]);
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     */
    public function testBadLabel() {
        $program = $this->getProgram();

        $response = static::createClient()->request(
            'POST',
            '/api/aids',
            [
                'headers' => [
                    'Content-Type' => 'application/json'
                ],
                'body' => json_encode(
                    [
                        'label' => null,
                        'amount' => 1000,
                        'program' => '/api/programs/' . $program->getId(),
                    ]
                )]
        );

        $this->assertResponseStatusCodeSame(400);
        $this->assertJsonContains([
            "@context" => "/api/contexts/Error",
            "@type" => "hydra:Error",
            "hydra:title" => "An error occurred",
            "hydra:description" => "The type of the \"label\" attribute must be \"string\", \"NULL\" given.",
        ]);
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     */
    public function testBadAmount() {
        $program = $this->getProgram();

        $response = static::createClient()->request(
            'POST',
            '/api/aids',
            [
                'headers' => [
                    'Content-Type' => 'application/json'
                ],
                'body' => json_encode(
                    [
                        'label' => 'Create new aid',
                        'amount' => -80,
                        'program' => '/api/programs/' . $program->getId(),
                    ]
                )]
        );

        $this->assertResponseStatusCodeSame(422);
        $this->assertJsonContains([
            "@context" => "/api/contexts/ConstraintViolationList",
            "@type" => "ConstraintViolationList",
            "hydra:title" => "An error occurred",
            "hydra:description" => "amount: This value should be greater than 0.",
            "violations" => [[
                    "propertyPath" => "amount",
                    "message" => "This value should be greater than 0.",
            ]]
        ]);
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     */
    public function testBadProgram() {
        $program = $this->getProgram();

        $response = static::createClient()->request(
            'POST',
            '/api/aids',
            [
                'headers' => [
                    'Content-Type' => 'application/json'
                ],
                'body' => json_encode(
                    [
                        'label' => 'Create new aid',
                        'amount' => 1000,
                        'program' => null,
                    ]
                )]
        );

        $this->assertResponseStatusCodeSame(400);
        $this->assertJsonContains([
            "@context" => "/api/contexts/Error",
            "@type" => "hydra:Error",
            "hydra:title" => "An error occurred",
        ]);
    }

    private function getProgram() : Program
    {
        $kernel = self::bootKernel();
        $entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $programRepository= $entityManager->getRepository(Program::class);
        $listPrograms = $programRepository->findAll();
        return $listPrograms[array_rand($listPrograms)];
    }
}