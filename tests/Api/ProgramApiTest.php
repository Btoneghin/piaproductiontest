<?php

namespace App\Tests\Api;

use ApiPlatform\Symfony\Bundle\Test\ApiTestCase;
use App\Entity\Program;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class ProgramApiTest extends ApiTestCase
{
    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     */
    public function testGetCollection(): void
    {
        $program = $this->getProgram();

        $response = static::createClient()->request(
            'GET',
            '/api/programs/region/' . $program->getRegion(),
            [
                'headers' => [
                    'Content-Type' => 'application/json'
                ]
            ]
        );

        $this->assertResponseIsSuccessful();
        $this->assertJsonContains([
            "@context" => "/api/contexts/Program",
            "@id" => "/api/programs/region/" . $program->getRegion(),
            "@type" => "hydra:Collection"
        ]);
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function testNoProgramForRegion() {
        $region = self::getRegionWithoutProgram();

        $response = static::createClient()->request(
            'GET',
            '/api/programs/region/' . $region,
            [
                'headers' => [
                    'Content-Type' => 'application/json'
                ]
            ]
        );

        $this->assertResponseIsSuccessful();
        $this->assertJsonContains([
            "@context" => "/api/contexts/Program",
            "@id" => '/api/programs/region/' . $region,
            "@type" => "hydra:Collection",
            "hydra:member" => [],
            "hydra:totalItems" => 0
        ]);
    }

    private function getProgram() : Program
    {
        $kernel = self::bootKernel();
        $entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $programRepository= $entityManager->getRepository(Program::class);
        $listPrograms = $programRepository->findAll();
        return $listPrograms[array_rand($listPrograms)];
    }

    private function getRegionWithoutProgram() : string
    {
        $listRegion = [
            "Auvergne-Rhône-Alpes",
            "Bourgogne-Franche-Comté",
            "Bretagne",
            "Centre-Val de Loire",
            "Corse",
            "Grand Est",
            "Hauts-de-France",
            "Ile-de-France",
            "Normandie",
            "Nouvelle-Aquitaine",
            "Occitanie",
            "Pays de la Loire",
            "Provence-Alpes-Côte d’Azur",
        ];

        $kernel = self::bootKernel();
        $entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager()
            ->getRepository(Program::class);

        $response = $entityManager->createQueryBuilder('p')
            ->where('p.dateStart < CURRENT_DATE()')
            ->andWhere('p.dateEnd > CURRENT_DATE()')
            ->getQuery()
            ->getArrayResult();

        $regionArray = array_column($response, "region");

        $result = array_diff($listRegion, $regionArray);
        return current($result);
    }
}