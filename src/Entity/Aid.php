<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Post;
use App\Repository\AidRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: AidRepository::class)]
#[ApiResource(
    operations: [new Post()]
)]
class Aid
{
    #[ORM\Id, ORM\GeneratedValue('AUTO'), ORM\Column]
    #[Groups('program')]
    private int $id;

    #[ORM\Column(type: Types::STRING,length: 255)]
    #[Assert\NotNull]
    #[Groups('program')]
    private string $label;

    #[ORM\Column]
    #[Assert\GreaterThan(0), Assert\NotNull]
    #[Groups('program')]
    private int $amount;

    #[ORM\ManyToOne(targetEntity: Program::class, inversedBy: 'aids')]
    private Program $program;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getAmount(): ?int
    {
        return $this->amount;
    }

    public function setAmount(int $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getProgram(): ?Program
    {
        return $this->program;
    }

    public function setProgram(?Program $program): self
    {
        $this->program = $program;

        return $this;
    }
}
