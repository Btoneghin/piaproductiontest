<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use App\Controller\Api\GetAvailableProgramController;
use App\Repository\ProgramRepository;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: ProgramRepository::class)]
#[UniqueEntity(fields: ['label', 'region'], message: 'Le programme existe déjà pour cette région.')]
#[ApiResource(
    operations: [
        new GetCollection(
            uriTemplate: '/programs/region/{region}',
            uriVariables: [
                'region' => 'string'
            ],
            controller: GetAvailableProgramController::class,
            openapiContext: [
                'description' => "Récupère l'ensemble des programmes disponibles pour la règion indiquée en paramètre"
            ],
            normalizationContext: ['groups' => ['program']]
        ),
        new Get(
            openapi: false
        )
    ],

)]
class Program
{
    #[ORM\Id, ORM\GeneratedValue, ORM\Column]
    #[Groups('program')]
    private int $id ;

    #[ORM\Column(type: Types::STRING, length: 255)]
    #[Assert\NotNull]
    #[Groups('program')]
    private string $label;

    #[ORM\Column(type: Types::DATE_MUTABLE)]
    #[Assert\NotNull, Assert\LessThan(propertyPath: 'dateEnd')]
    #[Groups('program')]
    private DateTimeInterface $dateStart;

    #[ORM\Column(type: Types::DATE_MUTABLE)]
    #[Assert\NotNull]
    #[Groups('program')]
    private DateTimeInterface $dateEnd;

    #[ORM\Column(type: Types::INTEGER)]
    #[Assert\GreaterThan(0)]
    #[Groups('program')]
    private int $budget;

    #[ORM\Column(type: Types::STRING,length: 255)]
    #[Assert\NotNull]
    #[Groups('program')]
    private string $region;

    #[ORM\OneToMany(mappedBy: 'program', targetEntity: Aid::class)]
    #[Assert\NotNull]
    #[Groups('program')]
    private Collection $aids;

    public function __construct()
    {
        $this->aids = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getDateStart(): ?DateTimeInterface
    {
        return $this->dateStart;
    }

    public function setDateStart(DateTimeInterface $dateStart): self
    {
        $this->dateStart = $dateStart;

        return $this;
    }

    public function getDateEnd(): ?DateTimeInterface
    {
        return $this->dateEnd;
    }

    public function setDateEnd(DateTimeInterface $dateEnd): self
    {
        $this->dateEnd = $dateEnd;

        return $this;
    }

    public function getBudget(): ?int
    {
        return $this->budget;
    }

    public function setBudget(int $budget): self
    {
        $this->budget = $budget;

        return $this;
    }

    public function getRegion(): ?string
    {
        return $this->region;
    }

    public function setRegion(string $region): self
    {
        $this->region = $region;

        return $this;
    }

    /**
     * @return Collection<int, Aid>
     */
    public function getAids(): Collection
    {
        return $this->aids;
    }

    public function addAid(Aid $aid): self
    {
        if (!$this->aids->contains($aid)) {
            $this->aids->add($aid);
            $aid->setProgram($this);
        }

        return $this;
    }

    public function removeAid(Aid $aid): self
    {
        if ($this->aids->removeElement($aid)) {
            // set the owning side to null (unless already changed)
            if ($aid->getProgram() === $this) {
                $aid->setProgram(null);
            }
        }

        return $this;
    }
}
