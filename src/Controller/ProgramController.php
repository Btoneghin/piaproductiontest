<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/program', name: 'app_program')]
class ProgramController extends AbstractController
{
    #[Route('/index', name: '_index')]
    public function index(): Response
    {
        return $this->render('program/index.html.twig');
    }
}
