<?php

namespace App\Controller;

use App\Entity\Aid;
use App\Form\AidType;
use App\Repository\AidRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/aid', name: 'app_aid')]
class AidController extends AbstractController
{

    #[Route('/index', name: '_index', methods: ['GET', 'POST'])]
    public function index(Request $request, AidRepository $aidRepository): Response
    {
        $aid = new Aid();
        $form = $this->createForm(AidType::class, $aid);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $aidRepository->save($aid, true);
            return $this->redirectToRoute('app_aid_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('aid/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
