<?php

namespace App\Controller\Api;

use App\Repository\ProgramRepository;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Attribute\AsController;

#[AsController]
class GetAvailableProgramController extends AbstractController
{

    private ProgramRepository $programRepository;
    private DateTime $now;

    public function __construct(ProgramRepository $programRepository)
    {
        $this->programRepository = $programRepository;
        $this->now = new DateTime();
    }

    public function __invoke(string $region): array
    {
        $listProgram = $this->programRepository->findBy(['region' => $region]);
        $return = [];
        foreach($listProgram as $program) {
            if($program->getDateStart() <= $this->now && ($program->getDateEnd() >= $this->now || $program->getDateEnd() === null )) {
                $return[] = $program;
            }
        }

        return $return;
    }
}