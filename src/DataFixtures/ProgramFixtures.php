<?php

namespace App\DataFixtures;

use App\Entity\Program;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker;

class ProgramFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create('fr_FR');
        $listDate = [
            'past' => [new DateTime('2021-01-01'), new DateTime('2021-12-31')],
            'actual' => [new DateTime('2022-01-01'), new DateTime('2022-12-31')],
            'future' => [new DateTime('2023-01-01'), new DateTime('2023-12-31')],
        ];
        $listRegion = [
            "Auvergne-Rhône-Alpes",
            "Bourgogne-Franche-Comté",
            "Bretagne",
            "Centre-Val de Loire",
            "Corse",
            "Grand Est",
            "Hauts-de-France",
            "Ile-de-France",
            "Normandie",
            "Nouvelle-Aquitaine",
            "Occitanie",
            "Pays de la Loire",
            "Provence-Alpes-Côte d’Azur",
        ];

        for ($i = 0; $i < 50; $i++) {
            $regions = $faker->randomElements($listRegion, $faker->numberBetween(0, count($listRegion)-1));
            foreach($regions as $region){
                $program = new Program();
                $program
                    ->setLabel('Programme ' . $i)
                    ->setBudget($faker->numberBetween(1, 9) * 1000000);
                $date = $faker->randomElement($listDate);
                $program
                    ->setDateStart($date[0])
                    ->setDateEnd($date[1]);
                $program->setRegion($region);
                $manager->persist($program);
            }
        }
        $manager->flush();
    }

}