<?php

namespace App\DataFixtures;


use App\Entity\Aid;
use App\Entity\Program;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker;

class AidFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager) : void
    {
        $faker = Faker\Factory::create('fr_FR');
        $programs = $manager->getRepository(Program::class)->findAll();

        foreach($programs as $program) {
            for($i = 0; $i < $faker->numberBetween(1, 5); $i++) {
                $aid = new Aid();
                $aid->setLabel('Aide '. $i)
                    ->setAmount($faker->numberBetween(1, 15) * 100)
                    ->setProgram($program);
                $manager->persist($aid);
            }
        }
        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [
            ProgramFixtures::class,
        ];
    }
}