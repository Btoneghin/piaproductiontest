<?php

namespace App\Form;

use App\Entity\Aid;
use App\Entity\Program;
use App\Repository\ProgramRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AidType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('label', TextType::class, ['label' => 'Nom'])
            ->add('amount', NumberType::class, ['label' => 'Montant'])
            ->add('program', EntityType::class, [
                'label' => 'Programme associé',
                'class' => Program::class,
                'choice_label' => 'label',
                ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Aid::class,
        ]);
    }
}
