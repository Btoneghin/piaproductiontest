# Test Technique PiaProduction

## Application
Petite application qui repond à la problématique du test technique. Elle comprend deux pages interfaces pour montrer
différentes techniques d'intégration de front-end (TWIG et VueJS dans notre cas)

### Partie Symfony
- Installation des vendors ``composer install``
- Lancer le server symfony ``symfony server:start`` ou utiliser un outil (WAMP par exemple)

#### Base de données
- Créer les fichiers ``.env.local`` et ``.env.test.local`` à la racine du projet et modifier les lignes DATABASE en 
utilisant votre configuration favorite 

#### Partie application
- Créer la base de données => ``php bin/console doctrine:database:create``
- Créer les tables => ``php bin/console doctrine:migrations:migrate``
- (optionnel) Créer des données de test => ``php bin/console doctrine:fixtures:load``

#### Partie API
- Swagger disponible via le lien suivant ``http://127.0.0.1:8000/api/docs`` 

#### Partie test
- Créer la base de données => ``php bin/console --env=test doctrine:database:create ``
- Créer les tables => ``php bin/console --env=test doctrine:schema:create ``
- (optionnel) Créer des données de test => ``php bin/console --env=test doctrine:fixtures:load``
- Execution des tests => `` php bin/phpunit ``

### Partie VueJS
- Installation yarn ``yarn install``
- Installation du build ``yarn encore dev``